﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public GameObject EnemyGO; //enemy prefab
    public GameObject meteoritoGO;
    float maxSpawnRateInSeconds = 4f;
	// Use this for initialization
	void Start () {
        

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnEnemy()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2 (0, 0)); //limit abaix esquerra

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)); //limit adalt dreta

        GameObject anEnemy = (GameObject)Instantiate(EnemyGO);
        GameObject anEnemy2 = (GameObject)Instantiate(meteoritoGO);
        anEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y); //instancia enemic
        anEnemy2.transform.position = new Vector2(Random.Range(min.x, max.x), max.y); //instancia enemic


        //funcio que fa que spawnejin enemics
        NextEnemySpawn();
    }



    void NextEnemySpawn()
    {
        float spawnInXSeconds;

        if (maxSpawnRateInSeconds > 1f)
        {
            //numero entre 1 i maxspawnrateseconds

            spawnInXSeconds = Random.Range(1f, maxSpawnRateInSeconds);

        }
        else

            spawnInXSeconds = 1f;
        Invoke("SpawnEnemy", spawnInXSeconds);
    }
           

        

        void IncreaseSpawnRate() //aumenta la dificultat en aumentar el spawn
        {
            if (maxSpawnRateInSeconds > 1f)
                maxSpawnRateInSeconds--;

            if (maxSpawnRateInSeconds == 1f)
                CancelInvoke("IncreaseSpawnRate");


        }

    public void StartSpawner()


    {
        //reinicia
        float maxSpawnRateInSeconds = 4f;
        Invoke("SpawnEnemy", maxSpawnRateInSeconds);

        //aumenta spawneo cada 20segons

        InvokeRepeating("IncreaseSpawnRate", 0f, 20f);
    }


    public void StopSpawner()
    {
        CancelInvoke("SpawnEnemy");
        CancelInvoke("IncreaseSpawnRate"); //parar el spawn
    }
    }

