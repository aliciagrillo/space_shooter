﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {

    public float speed;

    public GameObject GameManagerGO;
    public GameObject PlayerBulletGO; //pel prefab
    public GameObject bulletPosition01;
    public GameObject bulletPosition02;
    public GameObject ExplosionGO;

    //referencia a les vides ui texto

    public Text LivesUIText;

    int MaxLives = 3; //maxim vida
    int lives; // vides actuals

    


	// Use this for initialization
	void Start () {

        
        speed = 4;

        
	}

    public void Init()
    {
        lives = MaxLives;
        LivesUIText.text = lives.ToString(); //actualitza a traves string numero vides
        gameObject.SetActive(true); //activa


        transform.position = new Vector2(0, 0); //reiniciar posicio
    }
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown("space"))

        {

            //play el sound

            gameObject.GetComponent<AudioSource>().Play();


            //instanciar la bala &&fire
            GameObject bullet01 = (GameObject)Instantiate (PlayerBulletGO);
            bullet01.transform.position = bulletPosition01.transform.position;  // bala posicio inicial

            GameObject bullet02 = (GameObject)Instantiate (PlayerBulletGO);
            bullet02.transform.position = bulletPosition02.transform.position;



        }


        float x = Input.GetAxisRaw("Horizontal"); // moviment horitzontal que serà -1,0 o 1 
        float y = Input.GetAxisRaw("Vertical"); //Same -1,0,1  

        Vector2 direction = new Vector2 (x, y).normalized;

        Move(direction);

	}
    void Move (Vector2 direction)
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2 (0, 0));
        //limits
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        max.x = max.x - 0.0225f;  //resta la meitat del sprite
        min.x = min.x + 0.0225f;

        max.y = max.y - 0.0285f;  //resta la meitat del sprite
        min.y = min.y + 0.0285f;

        Vector2 pos = transform.position; // transforma la posicio del player 

        pos += direction * speed * Time.deltaTime; // calcula nova posicio

        pos.x = Mathf.Clamp(pos.x, min.x, max.x); //comprova que no hagi sortit del limit
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        transform.position = pos;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //detecta la colisio del player ship amb enemy ship o bullet

        if((col.tag == "EnemyShipTag") || (col.tag == "EnemyBulletTag" ))
        {
            PlayerExplosion();
            lives--; //resta vida
            LivesUIText.text = lives.ToString();

            if(lives <= 0) //si te 0 vides o menys
            {
                GameManagerGO.GetComponent<GameManager>().SetGameManagerState(GameManager.GameManagerState.GameOver);
                gameObject.SetActive(false); //amaga playership

                //canvia estat game manager


            }

        }
    }

    void PlayerExplosion()
    {

        GameObject explosion = (GameObject)Instantiate(ExplosionGO); //instancia

        explosion.transform.position = transform.position; //posicio explosio
    }
}
