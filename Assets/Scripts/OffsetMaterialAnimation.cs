﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OffsetMaterialAnimation : MonoBehaviour
{
    public Vector2 offset;
    public float smooth;

    private MeshRenderer meshRend;
	// Use this for initialization
	void Start ()
    {
        meshRend = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        offset.y += Time.deltaTime * smooth;

        MaterialPropertyBlock block = new MaterialPropertyBlock();

        meshRend.GetPropertyBlock(block);

        block.SetVector("_MainTex_ST",new Vector4(2.5f,5, offset.x, offset.y));

        meshRend.SetPropertyBlock(block);



        
	}
}
