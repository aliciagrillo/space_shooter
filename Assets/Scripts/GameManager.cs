﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject BackGO;
    public GameObject playButton;
    public GameObject playerShip;
    public GameObject enemyspawner;
    public GameObject GameOverGO;
    public GameObject scoreUITextGO;
    public GameObject TimeCounterGO;
    public GameObject GameTitleGO;
    public GameObject ExitButton;
    public GameObject CreditsButtonGO;

    public enum GameManagerState
    {
        Menu,
        Credits,
        Gameplay,
        GameOver,
    }
        GameManagerState GMState;
    
	// Use this for initialization
	void Start (){
        GMState= GameManagerState.Menu;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void UpdateGameManagerState()
    {
        switch(GMState)
        {
            case GameManagerState.Menu:
                //AMAGAR GAME OVER
                GameOverGO.SetActive(false);
                // POSAR BOTON PLAY VISIBLE
                playButton.SetActive(true);
                ExitButton.SetActive(true);
                CreditsButtonGO.SetActive(true);

                GameTitleGO.SetActive(true);
                break;

            case GameManagerState.Credits:
                GameOverGO.SetActive(false);
               
                playButton.SetActive(false);
                ExitButton.SetActive(true);
                CreditsButtonGO.SetActive(false);
                break;

            case GameManagerState.Gameplay:
                playButton.SetActive(false); //amaga buto de play al darle
                scoreUITextGO.GetComponent<GameScore>().Score = 0; //pts a 0 reset

                playerShip.GetComponent<PlayerControl>().Init();
                GameTitleGO.SetActive(false);

                ExitButton.SetActive(false);
                CreditsButtonGO.SetActive(false);
                

                enemyspawner.GetComponent<EnemySpawner>().StartSpawner(); //iniciar spawn
                TimeCounterGO.GetComponent<TimeCounter>().StartTimeCounter();


                break;

            case GameManagerState.GameOver:

                TimeCounterGO.GetComponent<TimeCounter>().StopTimeCounter();

                //parar el spawn
                enemyspawner.GetComponent<EnemySpawner>().StopSpawner();


                //ensenyar missatge gameover
                GameOverGO.GetComponent<AudioSource>().Play();
                GameOverGO.SetActive(true);
                Invoke("ChangeToMainState", 6f); // anira al menu dpres de 6segonds de morirnos 




                break;

        }
    }

    public void SetGameManagerState(GameManagerState state) //funcio per posar el game manager en una pantalla i sityuacio
    {
        GMState = state;
        UpdateGameManagerState();
    }

    public void StartGame()
    {
        GMState = GameManagerState.Gameplay;
        UpdateGameManagerState();

    }

    public void StartCredits()
    {
        GMState = GameManagerState.Credits;
        UpdateGameManagerState();

    }

    public void ChangeToMainState()
    {
        SetGameManagerState(GameManagerState.Menu);
    }
}
