﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarGen : MonoBehaviour {


    public GameObject StarGO;
    public int MaxStars;
	// Use this for initialization
	void Start () {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        for(int i = 0; i<MaxStars; i++)
        {
            GameObject star = (GameObject)Instantiate(StarGO);

            star.transform.position = new Vector2(Random.Range(min.x, max.x), Random.Range(min.y, max.y));



        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
