﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour {
    float speed;
	// Use this for initialization
	void Start () {
        speed = 8f;
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 position = transform.position; //agafa la posicio

        position = new Vector2(position.x, position.y + speed * Time.deltaTime); //nova posicio

        transform.position = position; //actualitza posicio

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)); //comprova el top xy maxim de la camera 

        if(transform.position.y > max.y) //chequeja si la bala el valor y es mes alt que el limit i el destrueix
        {
            Destroy(gameObject);

        }



	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "EnemyShipTag") //collisio entre bala i enemy shiop
        {
            Destroy(gameObject);
        }
    }
}
