﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour {

    public GameObject EnemyBulletGO;
	// Use this for initialization
	void Start () {
        Invoke("FireEnemyBullet", 1f); //dpres de 1s
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FireEnemyBullet()
    {

        //pilla referencia de la nostra nau
        GameObject playerShip = GameObject.Find("PlayerGO");

        if(playerShip != null) //si no esta mort
        {

            GameObject bullet = (GameObject)Instantiate(EnemyBulletGO); //instancia bullet

            bullet.transform.position = transform.position;

            Vector2 direction = playerShip.transform.position - bullet.transform.position; //direccio a la nau

            //direccio

            bullet.GetComponent<EnemyBullet>().SetDirection(direction);

        }

    }
}
