﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {
    float speed; //speed bala
    Vector2 _direction; //dirrecio de la bala

    bool isOK; //per saber la direccio de la bala

    void Awake()
    {
        speed = 5f;
        isOK = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	    

    public void SetDirection(Vector2 direction)
    {
        _direction = direction.normalized; // pilla direccio unitaria

        isOK = true;  //passa a true
    }
	// Update is called once per frame
	void Update () {
		if(isOK)
        {
            Vector2 position = transform.position; //pilla la posicio bala recent

            position += _direction * speed * Time.deltaTime; //nova posicio

            transform.position = position; //actualitza pos


            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
            //si la bala va fora de la pantalla es destrueix

        if((transform.position.x < min.x) || (transform.position.x > max.x) || (transform.position.y < min.y) || (transform.position.y < min.y))
            {
                Destroy (gameObject);
            }



        }
	}
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "PlayerShipTag") //collisio entre bala i playerShip
        {
            Destroy(gameObject);
        }
    }
}
