﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyControl : MonoBehaviour {

    GameObject scoreUITextGO;


    float speed; //velocitat enemic
    public GameObject ExplosionGO;


	// Use this for initialization
	void Start () {
        speed = 3f; //speed

        scoreUITextGO = GameObject.FindGameObjectWithTag("ScoreTextTag"); // pila score text ui

	}
	
	// Update is called once per frame
	void Update () {

        Vector2 position = transform.position; //pilla posicio actual

        position = new Vector2(position.x, position.y - speed * Time.deltaTime); // NOVA POSICIO

        transform.position = position; //ACTUALITZA POSICIO

        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)); // minim de abaix esquerra

        if(transform.position.y < min.y) //si surt del minim mor i explota
        {
            
            Destroy(gameObject);
        }
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if((col.tag == "PlayerShipTag") || (col.tag == "PlayerBulletTag")) //destrueix play ship si detecta colisio amb tags de player
        {
            PlayExplosion();

            scoreUITextGO.GetComponent<GameScore>().Score += 10; //suma 10pts cada cop q destrueix una nau

            Destroy(gameObject);
        }
    }
    void PlayExplosion()
    {
        GameObject explosion = (GameObject)Instantiate(ExplosionGO);

        explosion.transform.position = transform.position;
    }
}
